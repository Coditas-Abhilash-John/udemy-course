import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseIndexContainerComponent } from './course-index-container.component';

describe('CourseIndexContainerComponent', () => {
  let component: CourseIndexContainerComponent;
  let fixture: ComponentFixture<CourseIndexContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseIndexContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CourseIndexContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
