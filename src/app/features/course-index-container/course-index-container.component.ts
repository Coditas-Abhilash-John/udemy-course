import { Component, Input, OnInit } from '@angular/core';
import { ICourseDetails } from 'src/app/model/courseDetailsInterface';
import { IProgressRecord } from 'src/app/model/progressRecordInterface';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-course-index-container',
  templateUrl: './course-index-container.component.html',
  styleUrls: ['./course-index-container.component.scss'],
})
export class CourseIndexContainerComponent implements OnInit {
  constructor(private httpService: HttpService) {}

  courseDetails: ICourseDetails = {
    courseName: '',
    noOfLevels: 0,
    levels: [],
  };
  @Input() progressDetails: IProgressRecord = {
    userName: '',
    isCourseCompleted: false,
    levels: [],
  };

  async ngOnInit() {
    this.courseDetails = await this.httpService.getCourseDetails();
  }
}
