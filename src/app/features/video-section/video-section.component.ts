import { Component, Input, OnInit } from '@angular/core';
import { IVideoDetails } from 'src/app/model/courseDetailsInterface';

@Component({
  selector: 'app-video-section',
  templateUrl: './video-section.component.html',
  styleUrls: ['./video-section.component.scss'],
})
export class VideoSectionComponent implements OnInit {
  constructor() {}
  @Input() videosList: IVideoDetails[] = [];
  ngOnInit(): void {}
}
