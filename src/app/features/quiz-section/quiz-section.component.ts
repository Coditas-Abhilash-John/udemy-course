import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IQuestionDetails } from 'src/app/model/courseDetailsInterface';

@Component({
  selector: 'app-quiz-section',
  templateUrl: './quiz-section.component.html',
  styleUrls: ['./quiz-section.component.scss'],
})
export class QuizSectionComponent implements OnInit {
  @Input() quizList: IQuestionDetails[] = [];

  questionsCompletedStatus: boolean[] = [];
  @Output() quizCompleted = new EventEmitter();
  constructor() {}
  ngOnInit(): void {
    this.questionsCompletedStatus = new Array(this.quizList.length).fill(false);
  }
  currentQuestion: IQuestionDetails = {
    questionNo: 0,
    question: '',
    options: [],
    answer: '',
  };
  showQuestion(questionIndex: number) {
    this.currentQuestion = this.quizList[questionIndex];
  }
  recordQuestionResponse(response: any) {
    this.questionsCompletedStatus[response.questionNo - 1] = response.result;
    if (this.questionsCompletedStatus.every((status) => status)) {
      return this.quizCompleted.emit(true);
    }
  }
  getStyle(questionNo: number) {
    return {
      'completed-question': this.questionsCompletedStatus[questionNo - 1],
    };
  }
}
