import { Component, Input, OnInit } from '@angular/core';
import { IVideoDetails } from 'src/app/model/courseDetailsInterface';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-video-tile',
  templateUrl: './video-tile.component.html',
  styleUrls: ['./video-tile.component.scss'],
})
export class VideoTileComponent implements OnInit {
  constructor(private sanitizer: DomSanitizer) {}
  @Input() videoDetails: IVideoDetails = {
    videoNo: 0,
    videoID: '',
  };
  safeUrl: any;
  ngOnInit(): void {
    this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
      `https://www.youtube.com/embed/${this.videoDetails.videoID}`
    );
  }
}
