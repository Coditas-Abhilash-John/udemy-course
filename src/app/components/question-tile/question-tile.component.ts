import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IQuestionDetails } from 'src/app/model/courseDetailsInterface';

@Component({
  selector: 'app-question-tile',
  templateUrl: './question-tile.component.html',
  styleUrls: ['./question-tile.component.scss'],
})
export class QuestionTileComponent implements OnInit {
  @Input() questionDetails: IQuestionDetails = {
    questionNo: 0,
    question: '',
    options: [],
    answer: '',
  };
  @Output() questionResponse = new EventEmitter();

  form = new FormGroup({
    optionSelected: new FormControl('', [Validators.required]),
  });

  constructor() {}

  ngOnInit(): void {}
  evaluateQuestion(questionNo: number) {
    console.log({
      questionNo,
      result: this.form.value.optionSelected === this.questionDetails.answer,
    });
    return this.questionResponse.emit({
      questionNo,
      result: this.form.value.optionSelected === this.questionDetails.answer,
    });
  }
}
