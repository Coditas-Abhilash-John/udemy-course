import { Component, Input, OnInit } from '@angular/core';
import { INavigationDetails } from 'src/app/model/app.types';
import { ICourseLevelDetails } from 'src/app/model/courseDetailsInterface';
import {
  ILevelProgressRecord,
  IProgressRecord,
} from 'src/app/model/progressRecordInterface';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-level-tile',
  templateUrl: './level-tile.component.html',
  styleUrls: ['./level-tile.component.scss'],
})
export class LevelTileComponent implements OnInit {
  navigationDetails: INavigationDetails = {
    showCourseIntroPage: true,
  };

  constructor(private navigationService: NavigationService) {}

  @Input() levelDetails: ICourseLevelDetails = {
    levelNo: 0,
    levelName: '',
    noOfVideos: 0,
    videos: [],
    quiz: [],
  };
  @Input() levelProgressRecord: ILevelProgressRecord = {
    levelNo: 0,
    isLevelCompleted: false,
    isLevelAccessible: false,
    isQuizCompleted: false,
  };
  ngOnInit() {}
  getStyle(isLevelAccessible: boolean, isLevelCompleted: boolean) {
    return {
      'accessible-level': isLevelAccessible,
      'completed-level': isLevelCompleted,
    };
  }

  onClick() {
    this.navigationService.updatePath().emit(
      `/level/:${this.levelProgressRecord.levelNo}`
    );
  }
}
