import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class NavigationService {
  constructor() {}
  // instead of EventEmitter can we use Observables?
  private pathEvent = new EventEmitter<string>();

  updatePath() {
    return this.pathEvent;
  }
}
