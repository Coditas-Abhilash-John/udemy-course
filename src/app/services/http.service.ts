import { Injectable } from '@angular/core';
import { backendCourseDetails, backendProgressRecord } from 'src/mock/backend';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor() {}

  async getCourseDetails() {
    const courseDetails = await backendCourseDetails();
    return courseDetails;
  }
  async getProgressRecord(){
    const progressDetails = await backendProgressRecord();
    return progressDetails
  }
}
