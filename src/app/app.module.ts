import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CourseIntroComponent } from './pages/course-intro/course-intro.component';
import { CourseContentComponent } from './pages/course-content/course-content.component';
import { CourseIndexContainerComponent } from './features/course-index-container/course-index-container.component';
import { LevelTileComponent } from './components/level-tile/level-tile.component';
import { VideoSectionComponent } from './features/video-section/video-section.component';
import { VideoTileComponent } from './components/video-tile/video-tile.component';
import { QuizSectionComponent } from './features/quiz-section/quiz-section.component';
import { QuestionTileComponent } from './components/question-tile/question-tile.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CourseIntroComponent,
    CourseContentComponent,
    CourseIndexContainerComponent,
    LevelTileComponent,
    VideoSectionComponent,
    VideoTileComponent,
    QuizSectionComponent,
    QuestionTileComponent,
  ],
  imports: [BrowserModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
