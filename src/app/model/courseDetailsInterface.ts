export interface ICourseDetails {
  courseName: string;
  noOfLevels: number;
  levels: ICourseLevelDetails[];
}
export interface ICourseLevelDetails {
  levelNo: number;
  levelName: string;
  noOfVideos: number;
  videos: IVideoDetails[];
  quiz: IQuestionDetails[];
}

export interface IVideoDetails {
  videoNo: number;
  videoID: string;
}

export interface IQuestionDetails {
  questionNo: number;
  question: string;
  options: string[];
  answer: string;
}
