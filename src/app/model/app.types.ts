export interface INavigationDetails {
  showCourseIntroPage: boolean;
  showLevelNo?: number;
}
