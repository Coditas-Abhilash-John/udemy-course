export interface IProgressRecord {
  userName: string;
  isCourseCompleted: boolean;
  levels: ILevelProgressRecord[];
}

export interface ILevelProgressRecord {
  levelNo: number;
  isLevelCompleted: boolean;
  isLevelAccessible: boolean;
  isQuizCompleted: boolean;
}
