import { Component, Input, OnInit } from '@angular/core';
import {
  ILevelProgressRecord,
  IProgressRecord,
} from 'src/app/model/progressRecordInterface';

@Component({
  selector: 'app-course-intro',
  templateUrl: './course-intro.component.html',
  styleUrls: ['./course-intro.component.scss'],
})
export class CourseIntroComponent {
  @Input() ProgressDetails: IProgressRecord = {
    userName: '',
    isCourseCompleted: false,
    levels: [],
  };
}
