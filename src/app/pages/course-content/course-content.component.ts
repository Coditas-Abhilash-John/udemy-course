import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ICourseLevelDetails } from 'src/app/model/courseDetailsInterface';
import { ILevelProgressRecord } from 'src/app/model/progressRecordInterface';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-course-content',
  templateUrl: './course-content.component.html',
  styleUrls: ['./course-content.component.scss'],
})
export class CourseContentComponent implements OnInit {
  @Output() levelCompleted = new EventEmitter();
  @Input() levelDetails: ICourseLevelDetails = {
    levelNo: 0,
    levelName: '',
    noOfVideos: 0,
    videos: [],
    quiz: [],
  };
  constructor(private navigationService: NavigationService) {}

  @Input() levelProgressDetails: ILevelProgressRecord = {
    levelNo: 0,
    isLevelCompleted: false,
    isLevelAccessible: false,
    isQuizCompleted: false,
  };

  ngOnInit(): void {}

  onQuizCompletion(quizStatus: boolean) {
    this.levelProgressDetails.isQuizCompleted = quizStatus;
    this.levelProgressDetails.isLevelCompleted = true;
    console.log(this.levelProgressDetails);
    this.levelCompleted.emit(this.levelProgressDetails);
  }
  onClick() {
    this.navigationService.updatePath().emit('/');
  }
}
