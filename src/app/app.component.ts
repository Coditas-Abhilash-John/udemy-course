import { Component, OnInit } from '@angular/core';
import { INavigationDetails } from './model/app.types';
import { ICourseDetails, ICourseLevelDetails } from './model/courseDetailsInterface';
import {
  ILevelProgressRecord,
  IProgressRecord,
} from './model/progressRecordInterface';
import { HttpService } from './services/http.service';
import { NavigationService } from './services/navigation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'udemy-course';
  levelNo : number = 0;
  currentPath = '/';

  courseDetails: ICourseDetails = {
    courseName: '',
    noOfLevels: 0,
    levels: [],
  };

  progressDetails: IProgressRecord = {
    userName: '',
    isCourseCompleted: false,
    levels: [],
  };

  constructor(
    private httpService: HttpService,
    private navigationService: NavigationService
  ) {}

  async ngOnInit() {
    this.courseDetails = await this.httpService.getCourseDetails();
    this.progressDetails = await this.httpService.getProgressRecord();

    this.navigationService.updatePath().subscribe((endPoint: string) => {
      this.currentPath = `${endPoint}`;
      this.levelNo = parseInt(endPoint[endPoint.length-1])
    });
  }

  shouldActivate(path: string) {
    return path === this.currentPath;
  }

  onLevelCompleted(levelProgressRecord: ILevelProgressRecord) {
    this.progressDetails.levels[levelProgressRecord.levelNo - 1] =levelProgressRecord;
    this.progressDetails.levels[levelProgressRecord.levelNo].isLevelAccessible = true;
  }
}

