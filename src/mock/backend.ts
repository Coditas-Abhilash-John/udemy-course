import { ICourseDetails } from 'src/app/model/courseDetailsInterface';
import { IProgressRecord } from 'src/app/model/progressRecordInterface';


export const backendProgressRecord = async (): Promise<IProgressRecord> =>{
  return{
    userName : "User1",
    isCourseCompleted : false,
    levels : [
      {
        levelNo : 1,
        isLevelCompleted : false,
        isLevelAccessible : true,
        isQuizCompleted : false,
      },
      {
        levelNo : 2,
        isLevelCompleted : false,
        isLevelAccessible : false,
        isQuizCompleted : false,
      },
      {
        levelNo : 3,
        isLevelCompleted : false,
        isLevelAccessible : false,
        isQuizCompleted : false,
      },
      {
        levelNo : 4,
        isLevelCompleted : false,
        isLevelAccessible : false,
        isQuizCompleted : false,
      },
      {
        levelNo : 5,
        isLevelCompleted : false,
        isLevelAccessible : false,
        isQuizCompleted : false,
      },
      {
        levelNo : 6,
        isLevelCompleted : false,
        isLevelAccessible : false,
        isQuizCompleted : false,
      },
      {
        levelNo : 7,
        isLevelCompleted : false,
        isLevelAccessible : false,
        isQuizCompleted : false,
      },
      {
        levelNo : 8,
        isLevelCompleted : false,
        isLevelAccessible : false,
        isQuizCompleted : false,
      },
      {
        levelNo : 9,
        isLevelCompleted : false,
        isLevelAccessible : false,
        isQuizCompleted : false,
      },
      {
        levelNo : 10,
        isLevelCompleted : false,
        isLevelAccessible : false,
        isQuizCompleted : false,
      }
    ]
  }
}


export const backendCourseDetails = async (): Promise<ICourseDetails> => {
  return {
    courseName: 'Angular',
    noOfLevels: 10,
    levels: [
      {
        levelNo: 1,
        levelName: 'Level 1',
        noOfVideos: 2,
        videos: [
          {
            videoNo: 1,
            videoID : "wXzfoeIXW5w",
          },
          {
            videoNo: 2,
            videoID : "SEAKuerLbsk",
          },
        ],
        quiz: [
          {
            questionNo:1,
            question: "1+1 = ?",
            options : ["5","3","8","2"],
            answer: "2"
          },
          {
            questionNo:2,
            question: "5+1 = ?",
            options: ["5","6","8","12"],
            answer: "6"
          }
        ],
      },
      {
        levelNo: 2,
        levelName: 'Level 2',
        noOfVideos: 2,
        videos: [
          {
            videoNo: 1,
            videoID : "SEAKuerLbsk",
          },
          {
            videoNo: 2,
            videoID : "wXzfoeIXW5w",
          },
        ],
        quiz: [
          {
            questionNo:1,
            question: "1+1 = ?",
            options : ["5","3","8","2"],
            answer: "2"
          },
          {
            questionNo:2,
            question: "5+1 = ?",
            options: ["5","6","8","12"],
            answer: "6"
          }
        ],
      },
      {
        levelNo: 3,
        levelName: 'Level 3',
        noOfVideos: 2,
        videos: [
          {
            videoNo: 1,
            videoID : "SEAKuerLbsk",
          },
          {
            videoNo: 2,
            videoID : "wXzfoeIXW5w",
          },
        ],
        quiz: [
          {
            questionNo:1,
            question: "1+1 = ?",
            options : ["5","3","8","2"],
            answer: "2"
          },
          {
            questionNo:2,
            question: "5+1 = ?",
            options: ["5","6","8","12"],
            answer: "6"
          }
        ],
      },
      {
        levelNo: 4,
        levelName: 'Level 4',
        noOfVideos: 2,
        videos: [
          {
            videoNo: 1,
            videoID : "wXzfoeIXW5w",
          },
          {
            videoNo: 2,
            videoID : "SEAKuerLbsk",
          },
        ],
        quiz: [
          {
            questionNo:1,
            question: "1+1 = ?",
            options : ["5","3","8","2"],
            answer: "2"
          },
          {
            questionNo:2,
            question: "5+1 = ?",
            options: ["5","6","8","12"],
            answer: "6"
          }
        ],
      },
      {
        levelNo: 5,
        levelName: 'Level 5',
        noOfVideos: 2,
        videos: [
          {
            videoNo: 1,
            videoID : "wXzfoeIXW5w",
          },
          {
            videoNo: 2,
            videoID : "SEAKuerLbsk",
          },
        ],
        quiz: [
          {
            questionNo:1,
            question: "1+1 = ?",
            options : ["5","3","8","2"],
            answer: "2"
          },
          {
            questionNo:2,
            question: "5+1 = ?",
            options: ["5","6","8","12"],
            answer: "6"
          }
        ],
      },
      {
        levelNo: 6,
        levelName: 'Level 6',
        noOfVideos: 2,
        videos: [
          {
            videoNo: 1,
            videoID : "wXzfoeIXW5w",
          },
          {
            videoNo: 2,
            videoID : "SEAKuerLbsk",
          },
        ],
        quiz: [
          {
            questionNo:1,
            question: "1+1 = ?",
            options : ["5","3","8","2"],
            answer: "2"
          },
          {
            questionNo:2,
            question: "5+1 = ?",
            options: ["5","6","8","12"],
            answer: "6"
          }
        ],
      },
      {
        levelNo: 7,
        levelName: 'Level 7',
        noOfVideos: 2,
        videos: [
          {
            videoNo: 1,
            videoID : "wXzfoeIXW5w",
          },
          {
            videoNo: 2,
            videoID : "SEAKuerLbsk",
          },
        ],
        quiz: [
          {
            questionNo:1,
            question: "1+1 = ?",
            options : ["5","3","8","2"],
            answer: "2"
          },
          {
            questionNo:2,
            question: "5+1 = ?",
            options: ["5","6","8","12"],
            answer: "6"
          }
        ],
      },
      {
        levelNo: 8,
        levelName: 'Level 8',
        noOfVideos: 2,
        videos: [
          {
            videoNo: 1,
            videoID : "wXzfoeIXW5w",
          },
          {
            videoNo: 2,
            videoID : "SEAKuerLbsk",
          },
        ],
        quiz: [
          {
            questionNo:1,
            question: "1+1 = ?",
            options : ["5","3","8","2"],
            answer: "2"
          },
          {
            questionNo:2,
            question: "5+1 = ?",
            options: ["5","6","8","12"],
            answer: "6"
          }
        ],
      },
      {
        levelNo: 9,
        levelName: 'Level 9',
        noOfVideos: 2,
        videos: [
          {
            videoNo: 1,
            videoID : "wXzfoeIXW5w",
          },
          {
            videoNo: 2,
            videoID : "SEAKuerLbsk",
          },
        ],
        quiz: [
          {
            questionNo:1,
            question: "1+1 = ?",
            options : ["5","3","8","2"],
            answer: "2"
          },
          {
            questionNo:2,
            question: "5+1 = ?",
            options: ["5","6","8","12"],
            answer: "6"
          }
        ],
      },
      {
        levelNo: 10,
        levelName: 'Level 10',
        noOfVideos: 2,
        videos: [
          {
            videoNo: 1,
            videoID : "wXzfoeIXW5w",
          },
          {
            videoNo: 2,
            videoID : "SEAKuerLbsk",
          },
        ],
        quiz: [
          {
            questionNo:1,
            question: "1+1 = ?",
            options : ["5","3","8","2"],
            answer: "2"
          },
          {
            questionNo:2,
            question: "5+1 = ?",
            options: ["5","6","8","12"],
            answer: "6"
          }
        ],
      }
    ],
  };
};
